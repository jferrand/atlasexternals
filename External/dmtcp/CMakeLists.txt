# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building DMTCP for ATLAS.
#

# The package's name:
atlas_subdir( dmtcp )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/dmtcpBuild )

# Set up the build of DMTCP:
ExternalProject_Add( dmtcp
   PREFIX ${CMAKE_BINARY_DIR}
   GIT_REPOSITORY https://github.com/dmtcp/dmtcp
   GIT_TAG 2.4.5
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ./configure --prefix=${_buildDir}
   BUILD_COMMAND make
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/
   ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
add_dependencies( Package_dmtcp dmtcp )

# Install DMTCP:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install the CMake module(s) from the package:
install( FILES cmake/Finddmtcp.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules OPTIONAL )
