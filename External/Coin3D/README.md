# Coin3D

## Intro

This is the Coin3D library, an open source clone of the Open Inventor 3D graphics library originally developed by SIG.

The library is used by the ATLAS **VP1** event display as main 3D graphics engine.
Moreover, the glue library **SoQt** is built against it as well.

## About this External package 

Usually, in ATLAS, we use the version of Coin3D shipped with the LCG software release. But we recently observed severe issues while compiling Coin3D with GCC 6.2.
To speed up the fix of this library, we created this External package, to be able to quickly patch the source code for our purposes.

The source code compiled here is taken from the HEAD (at the time of writing) of the `CMake` branch of the community-maintained sources of Coin3D (which can be found here:
https://bitbucket.org/Coin3D/coin/src/1ed29953c095?at=CMake) Then, a very recent patch from Fedora is applied, to fix the issues with GCC 6.2

More details about the issue and about the fix:

* https://its.cern.ch/jira/browse/ATLASVPONE-347
* https://bitbucket.org/Coin3D/coin/issues/128/crash-in-cc_memalloc_deallocate
* https://bugzilla.redhat.com/show_bug.cgi?id=1323159

When LCG will implement the needed fixes, we can move back to use that version.


